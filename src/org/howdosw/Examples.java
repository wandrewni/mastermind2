package org.howdosw;

/**
 * Created by anicholson on 2/27/17.
 */
public class Examples {
    public static void main(String[] args) {
        Examples.encapsulation_makes_it_easy_to_make_bad_words();
    }

    // a.k.a. "information hiding"
    public static void encapsulation_makes_it_easy_to_make_bad_words(){
        char helloChars[] = {'h','e','l','l','o'};
        String hello = "hello";

        // w/ encapsulation
        System.out.println("with string: " + hello.substring(0,4));

        System.out.println("classes hide the low-level details of their internal data");
        System.out.println("AND only provide limited access via specific behaviors (methods)");


        // w/o encapsulation
        System.out.print("with chars: " + helloChars[0] + helloChars[1] + helloChars[2] + helloChars[3]);
    }
}
